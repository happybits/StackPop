// ==UserScript==
// @name         Stack Pop
// @namespace    https://codeberg.org/happybits/stack-pop
// @version      1.2
// @description  Adds a the first StackOverflow answer to your search query
// @author       happybits 
// @match        https://www.google.com/search*
// @icon
// @grant        GM_xmlhttpRequest
// @require      file://C:\Project\StackPop\tampermonkey.google.js
// ==/UserScript==

