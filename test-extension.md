# Testing StackPop

The links below can be used to see if StackPop still works as expected after the code has been updated

## StackOverflow

Here you should probably get a result from StackOverflow and the StackPop userscript will be triggered:

https://www.google.com/search?q=javascript+open+new+tab
https://www.google.com/search?q=javascript+replace+regex

https://www.google.com/search?q=c%23+open+file
https://www.google.com/search?q=c%23+split+string

https://www.google.com/search?q=java+convert+string+to+int
https://www.google.com/search?q=java+array+initialization

https://www.google.com/search?q=Full+text+search+for+a+static+HTML+site

## Reddit

https://www.google.com/search?q=do+i+need+radeon+software
https://www.google.com/search?q=I%27ve+worked+with+NextJS+for+around+a+year+now

https://www.google.com/search?q=I+started+a+new+job+this+week+and+shipped+this+gorgeous+settings+UI+yesterday
https://www.google.com/search?q=reddit+Alternatives+to+Heroku

## Both

https://www.google.com/search?q=Do+we+really+need+to+wrap+every+function+in+useCallback+or+useMemo